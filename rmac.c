/* Copyright (c) 2023 Leah Rowe <info@minifree.org> */
/* SPDX-License-Identifier: MIT */

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int setMacAddress(void);
uint8_t hextonum(char chs);
uint8_t rhex(void);
void showmac(void);
void xpledge(const char *promises, const char *execpromises);
void xunveil(const char *path, const char *permissions);

uint16_t mac[3] = {0, 0, 0};
int rfd, rval;

const char *strMac = NULL, *strRMac = "??:??:??:??:??:??";

#define ERR() errno = errno ? errno : ECANCELED

int
main(int argc, char *argv[])
{
	xpledge("stdio rpath unveil", NULL);
	xunveil("/dev/urandom", "r");
	if ((rfd = open("/dev/urandom", O_RDONLY)) == -1)
		err(ERR(), "/dev/urandom");
	xpledge("stdio", NULL);

	strMac = (argc < 2) ? strRMac : argv[1];
	if ((rval = setMacAddress())) {
		strMac = strRMac;
		mac[0] = mac[1] = mac[2] = 0;
		(void)setMacAddress();
	}
	showmac();
	return rval ? rval : errno;
}

int
setMacAddress(void)
{
	uint64_t total = 0;
	if (strnlen(strMac, 20) == 17) {
	for (uint8_t h, i = 0; i < 16; i += 3) {
		if (i != 15)
			if (strMac[i + 2] != ':')
				return 1;
		int byte = i / 3;
		for (int nib = 0; nib < 2; nib++, total += h) {
			if ((h = hextonum(strMac[i + nib])) > 15)
				return 1;
			if ((byte == 0) && (nib == 1))
				if (strMac[i + nib] == '?')
					h = (h & 0xE) | 2; /* local, unicast */
			mac[byte >> 1] |= ((uint16_t ) h)
			    << ((8 * (byte % 2)) + (4 * (nib ^ 1)));
		}
	}}
	return ((total == 0) | (mac[0] & 1)); /* multicast/all-zero banned */
}

uint8_t
hextonum(char ch)
{
	if ((ch >= '0') && (ch <= '9'))
		return ch - '0';
	else if ((ch >= 'A') && (ch <= 'F'))
		return ch - 'A' + 10;
	else if ((ch >= 'a') && (ch <= 'f'))
		return ch - 'a' + 10;
	return (ch == '?') ? rhex() : 16;
}

uint8_t
rhex(void)
{
	static uint8_t n = 0, rnum[16];
	if (!n)
		if (read(rfd, (uint8_t *) &rnum, (n = 15) + 1) == -1)
			err(ERR(), "/dev/urandom");
	return rnum[n--] & 0xf;
}

void
showmac(void)
{
	for (int c = 0; c < 3; c++) {
		uint16_t val16 = mac[c];
		printf("%02x:%02x", val16 & 0xff, val16 >> 8);
		printf(c == 2 ? "\n" : ":");
	}
}

void
xpledge(const char *promises, const char *execpromises)
{
	(void)promises; (void)execpromises;
#ifdef __OpenBSD__
	if (pledge(promises, execpromises) == -1)
		err(ERR(), "pledge");
#endif
}

void
xunveil(const char *path, const char *permissions)
{
	(void)path; (void)permissions;
#ifdef __OpenBSD__
	if (unveil(path, permissions) == -1)
		err(ERR(), "unveil");
#endif
}
